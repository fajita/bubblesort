
class BubbleSort{
	
	public static void main(String [] args){
		if(args.length > 0){
			System.out.print(String.format("%d args are passed\n", args.length));
		}else{
			System.out.print("bubblesort\n");
		}
		
		//int[] arr = {3, 2, 8, 7, 4, 6, 1, 0, 5, 9};
		int[] arr = {
		3, 2, 8, 7, 4, 6, 1, 0, 5, 9, 10, 3, 9, 12
		};
		BubbleSort sortalgorithm = new BubbleSort();
		sortalgorithm.print(arr);
		sortalgorithm.sort(arr);
		sortalgorithm.print(arr);
		
	}
	
	public BubbleSort(){
		
	}
	
	public void print(int [] ary){
		for(int x : ary){
			System.out.print(String.valueOf(x) + ", ");
		}
		System.out.println();
	}
	
	public void sort(int [] ary){
		int tmp, swap = 0;
		while(true){
			swap = 0;
			for(int i=0; i<ary.length-1; i++){
				if(ary[i] > ary[i+1]){
					//swap(ary[i], ary[i+1]);
					tmp = ary[i];
					ary[i] = ary[i+1];
					ary[i+1] = tmp;
					swap++;
				}
			}
			if(swap == 0) break;
		}
	}
}